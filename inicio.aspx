<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="INICIO.aspx.cs" Inherits="WebApplication4.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<html>
	<head>
        <title>PAGINA MAUricio CH</title>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	    <style type="text/css">
            .auto-style1 {
                border-style: none;
                border-color: inherit;
                border-width: 0;
                -moz-appearance: none;
                -webkit-appearance: none;
                -ms-appearance: none;
                appearance: none;
                -moz-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
                -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
                -ms-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
                transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
                background-color: #666;
                border-radius: 6px;
                color: #ffffff;
                cursor: pointer;
                display: inline-block;
                font-weight: 400;
                height: 3em;
                line-height: 3em;
                padding: 0 2em;
                text-align: center;
                text-decoration: none;
                white-space: nowrap;
                left: 2px;
                top: -12px;
            }
        </style>
	</head>
	<body class="landing is-preload">
		<form id="form1" runat="server">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
					<h1><a href="index.html">Lbreria UPDS</a>#1 </h1>
					<nav id="nav">
						<ul>
							<li><a href="inicio.aspx">INICIO</a></li>
							<li>
								&nbsp;<ul>
									<li><a href="informacion.html">INFORMACION</a></li>
									<li><a href="contactenos.html">CONTACTENOS</a></li>
									<li><a href="carreras.html">CARRERAS</a></li>
									<li>
										<a href="#">OTROS</a>
										<ul>
											<li><a href="#">CAMPUS</a></li>
											<li><a href="#">MISION</a></li>
											<li><a href="#">VISION</a></li>
											<li><a href="#">IMAGENES</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<asp:Button ID="Button1" runat="server" Text="CLIENTE" PostBackUrl="DOCENTE.aspx" />
							<li><a href="REGISTRO.aspx" class="auto-style1">REGISTRAR</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>INICIO</h2>
					<p>La primera libreria online de Bolivia a de tu alcance, con un solo click.</p>
					<ul class="actions special">
						<li></li>
						<li></li>
					</ul>
				</section>

			<!-- Main -->
				<section id="main" class="container">

					<section class="box special">
						<header class="major">
							<h2>E<span lang="ES-BO">l primer portal de venta de libros de Bolivia</span></h2>
							<p>A<span lang="ES-BO">portando al crecimiento de la juventud y niñez boliviana.</span></p>
						</header>
						<span class="image featured"><img src="images/pic01.jpg" alt="" /></span>
					</section>

					<section class="box special features">
						<div class="features-row">
							<section>
								<span class="icon major fa-bolt accent2"></span>
								<h3>CIENTIFICOS</h3>
								<p>Libros de ciencia (del latín scientĭa, ‘conocimiento’) es un sistema ordenado de conocimientos estructurados que estudia, investiga e interpreta los fenómenos naturales, sociales y artificiales.. </p>
							</section>
							<section>
								<span class="icon major fa-area-chart accent3"></span>
								<h3>Literatura y lingüísticos</h3>
								<p>Según la Real Academia Española (RAE), literatura es el «arte de la expresión verbal»1​ (entendiéndose como verbal aquello «que se refiere a la palabra, o se sirve de ella»2​) y, por lo tanto, abarca tanto textos escritos (literatura escrita) como hablados o cantados (literatura oral).</p>
							</section>
						</div>
						<div class="features-row">
							<section>
								<span class="icon major fa-cloud accent4"></span>
								<h3>Biografías</h3>
								<p>La biografía es una historia de la vida de una persona narrada por otra persona, es decir, en pleno sentido desde su nacimiento hasta su muerte, mencionando sus logros y fracasos o también si se involucró en un evento importante para la humanidad. Existen varios tipos de biografía como la "informativa", "personal" o "crítica".</p>
							</section>
							<section>
								<span class="icon major fa-lock accent5"></span>
								<h3>De viaje</h3>
								<p>Libro de viaje, como género literario, es la exposición de las experiencias y observaciones realizadas por un viajero, y puede acompañarse de mapas, dibujos, grabados, fotografías, etcétera, realizadas por el autor o por sus compañeros de viaje.</p>
							</section>
						</div>
					</section>

					<div class="row">
						<div class="col-6 col-12-narrower">

							<section class="box special">
								<span class="image featured"><img src="images/pic02.jpg" alt="" /></span>
								<h3>THE  ARRIVALS</h3>
								<p>¿Qué es lo que lleva a tanta gente a dejarlo todo atrás para viajar hacia un país desconocido, un lugar en el que no tienen familia ni amigos, donde nada tiene nombre y el futuro es una incógnita?.</p>
								<ul class="actions special">
									<li><a href="#" class="button alt">LEE mas</a></li>
								</ul>
							</section>

						</div>
						<div class="col-6 col-12-narrower">

							<section class="box special">
								<span class="image featured"><img src="images/pic03.jpg" alt="" /></span>
								<h3>LAVANDERA</h3>
								<p>La historia del poeta mexicano Manuel Acuña desde la mirada enamorada de su lavandera. ¿La literatura como quitamanchas?. A partir del suicidio del joven poeta mexicano Manuel Acuña, Soledad, lavandera de origen gallego, cuenta su propia vida y la de él percibida a través de lo que, cual detective de la ropa sucia, descubre por sus manchas. Pepe Monteserín se asoma a la intrahistoria, a la cotidianidad, para narrar el devenir de una lavandera en el México del siglo XIX y su relación de abnegación y afectos ocasionales con el malogrado poeta. </p>
								<ul class="actions special">
									<li><a href="#" class="button alt">LEE mas</a></li>
								</ul>
							</section>

						</div>
					</div>

				</section>

			<!-- CTA -->
				<section id="cta">

					<h2>SORTEOS &nbsp;</h2>
					<p>Deje su correo electronico para los sorteos de libros.</p>

						<div class="row gtr-50 gtr-uniform">
							<div class="col-8 col-12-mobilep">
								<input type="email" name="email" id="email" placeholder="correo electronico" />
							</div>
							<div class="col-4 col-12-mobilep">
								<input type="submit" value="ACEPTAR" class="fit" />
							</div>
						</div>

				</section>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>� </li>
						<li>Derechos reservados libreria UPDS #1</li>
                        <li>.</li><li>Design: <a href="http://html5up.net">Mauricio Ch</a></li>
                        <li></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
	WebForm1.aspx</form>
@*  *@